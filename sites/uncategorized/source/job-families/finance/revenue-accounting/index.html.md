---
layout: job_family_page
title: Revenue Accounting
description: "The Revenue Accounting Team at Gitlab form part of the Revenue Accounting Group. The Revenue Accounting Team is responsible for Worldwide Revenue Assurance & Accounting."
---

The Revenue Accounting Team at Gitlab form part of the Revenue Accounting Group. The Revenue Accounting Team is responsible for Worldwide Revenue Assurance & Accounting. 

## Revenue Accounting Team Levels

### Associate Revenue Accountant

The Associate Revenue Accountant will report to the Manager, Revenue Accounting or above. 


#### Associate Revenue Accountant Job Grade

The Associate Revenue Accountant is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades)

#### Associate Revenue Accountant Responsibilities 

* Learn and apply ASC 606 and company revenue recognition policies. 
* Assist in the review of customer contracts for proper revenue recognition in accordance with company policy and revenue recognition accounting standards. 
* Assist in the research and documentation of accounting issues.  
* Assist with monthly balance sheet account reconciliations and journal entries for month end close. 
* Assist in standalone selling price analysis. 
* Work on ad hoc projects as required. 

#### Associate Revenue Accountant Requirements

* Bachelor degree or equivalent experience (Accounting, Business, Finance or equivalent degree preferred)
* Proficient with excel and google sheets
* Ability to work effectively both independently and with a team, assist with a variety of projects of varying degrees of complexity
* Leverage knowledge gained through education and on-the-job experience as well internal and external training and coaching to perform essential functions of the job
* Strong analytical skills and attention to detail
* You share our values, and work in accordance with those values.
* Ability to use GitLab

### Revenue Accountant

The Revenue Accountant will report to the Manager, Revenue Accounting or above. 

#### Revenue Accountant Job Grade

The Revenue Accountant is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Revenue Accountant Responsibilities

* Review of customer contracts for proper revenue recognition in accordance with company policy and revenue recognition accounting standards. 
* Research and document accounting issues and recommend solutions as needed. 
* Prepare monthly balance sheet account reconciliations and journal entries and contribute to the month end close process. 
* Perform standalone selling price analysis. 
* Ensure compliance with internals controls and processes
* Work on ad hoc projects as required. 

#### Revenue Accountant Requirements

* Bachelor degree or equivalent experience (Accounting, Business, Finance or equivalent degree preferred)
* 2 years of related accounting experience, public company accounting experience is a plus
* Flexible to meet changing priorities and the ability to prioritize workload to achieve on time accurate results

### Senior Revenue Accountant

The Senior Revenue Accountant will report to the Manager, Revenue Accounting or above.  

#### Senior Revenue Accountant Job Grade

The Senior Revenue Accountant is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Revenue Accountant Responsibilities

* Review and summarize customer contracts for proper revenue recognition in accordance with company policy and revenue recognition accounting standards
* Key contributor in the monthly financial close, ensuring accurate and timely recording of transactions and the completeness of financial statements
* Work closely and help manage transactions with the billing and deal desk teams
* Perform standalone selling price
* Work on ad hoc projects including implementation of new systems and standards
* Work towards becoming a subject matter expert on revenue and ASC 606. 
* Support training and review of junior team members. 

#### Senior Revenue Accountant Requirements

* Extends the Revenue Accountant (Intermediate) Responsibilities
* Strong working knowledge of US GAAP principles and financial statements, including ASC 606 and multi-element experience
* 3-5 years of related accounting experience, public company accounting experience is a plus
* Flexible to meet changing priorities and the ability to prioritize workload to achieve on time accurate results
* Experience with Netsuite, Zuora or Revpro considered an advantage
* International experience preferred.
* Detail-oriented, self-directed and able to effectively complete tasks with minimal supervision

### Manager, Revenue Accounting

The Manager, Revenue Accounting will report to the Senior Manager, Revenue Accounting or above.


#### Manager, Revenue Accounting Job Grade

The Manager, Revenue Accounting is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Manager, Revenue Accounting Responsibilities

* Review and approve deal review packs in accordance with review thresholds.
* Lead the research and documentation of accounting issues and recommend solutions for the most complex transactions as needed
* Lead the monthly financial close, ensuring accurate and timely recording of transactions and the completeness of financial statements
* Work closely with billing, legal and sales on structuring of contracts 
* Review month end balance sheet account reconciliations and journal entries and contribute to the financial reporting process
* Analysis of standalone selling price
* Work on special revenue projects including implementation of new systems and standards
* Report on and analyze revenue results
* Identify and develop best practices and process improvements
* Guide and manage the revenue team
* Liaison with External auditors, present significant item updates, supporting documentation for the quarterly reviews and year-end audits

#### Manager, Revenue Accounting Requirements

* Strong working knowledge of US GAAP principles and financial statements, including ASC 606 and multi-element experience
* 7-10 years of related accounting experience, public company accounting experience is a plus
* Ability to manage people
* Experience with managing a team
* Strong ability to work well within a team structure and an ability to influence, train, mentor and leverage the skills of others to achieve objectives
* Flexible to meet changing priorities and the ability to prioritize workload to achieve on time accurate results

### Senior Manager, Revenue Accounting

The Senior Manager, Revenue Accouting reports to the Director, Revenue Accounting or above.

#### Senior Manager, Revenue Job Grade

The Senior Manager, Revenuer Accouting is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Manager, Revenue Accounting Responsibilities

* Review and approve deal review packs in accordance with review thresholds.
* Review and summarize customer contracts for proper revenue recognition in accordance with company policy and revenue recognition accounting standards
* Take the leading role in the research and documentation of accounting issues and recommend solutions for the most complex transactions as needed
* Take the leading role in the monthly financial close, ensuring accurate and timely recording of transactions and the completeness of financial statements
* Work closely with billing, legal and sales on structuring of contracts 
Review month end balance sheet account reconciliations and journal entries and contribute to the financial reporting process
* Ensure compliance with internals controls and processes
* Analysis of standalone selling price
* Work on special revenue projects including implementation of new systems and standards
* Report on, analyze and provide commentary on revenue results
* Identify and develop best practices and process improvements
* Guide, manage and mentor the revenue team
* Ensure all team member are adequately trained and managed
Subject matter expert and trusted business advisor on revenue and ASC 606
* Liaison with External auditors, present significant item updates, supporting documentation for the quarterly reviews and year-end audits
* Support the ongoing revenue automation process, and work with IT to prioritize further potential system enhancements and development projects related to revenue

#### Senior Manager, Revenue Accouting Requirements

* Strong working knowledge of US GAAP principles and financial statements, including ASC 606 and multi-element experience
* 10-12 years of related accounting experience, public company accounting experience is a plus
* 3-5 years of experience in managing a team
* Polished communication skills, including an ability to listen to the needs of the business units, research and comprehend complex matters, articulate issues in a clear and simplified manner, and present findings and recommendations in both oral and written presentations
* Strong ability to work well within a team structure and an ability to influence, train, mentor and leverage the skills of others to achieve objectives
* Flexible to meet changing priorities and the ability to prioritize workload to achieve on time accurate results

### Performance Indicators

- [Average days to close](/handbook/finance/accounting/#month-end-review--close)
- [Number of material audit adjustments](/handbook/internal-audit/#performance-measures-for-accounting-and-finance-function-related-to-audit)
- [Percentage of ineffective Sox Controls](/handbook/internal-audit/#performance-measures-for-accounting-and-finance-function-related-to-audit)

## Director, Revenue Accounting Levels

### Director, Revenue Accounting

The Director, Revenue Accounting reports to the Principal Accounting Offer (PAO).

#### Director, Revenue Accounting Job Grade

The Director, Revenue Accounting is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Director, Revenue Accounting Responsibilities

* Guide, mentor and manage the world-wide revenue, billing and commission teams
* Matrix management capabilities to communicate with and educate finance and non-finance personnel on revenue accounting guidance, Gitlab specific policies and procedures as well as inputs on structuring of customer arrangements
* Review customer arrangements for proper revenue recognition in accordance with company policy and revenue recognition accounting standards
* Ensure the integrity of the world-wide license, support, service and training revenue and assist in new product offerings that may be developed over time
* Review license and service revenue related balance sheet account reconciliations
* Be a global partner with subject matter expertise in revenue and work diligently on finding solutions to critical issues
* Develop and maintain strong collaborative working relationships across Customer Service, Legal, Sales, FP&A, Accounting and Tax teams
* Research and review technical revenue accounting memos that addresses the issue, and provide analysis and conclusions related to various customer contracts or new technical revenue pronouncements
* Ensure the integrity of key processes by understanding systems, flow of transactions, internal controls, and recommending efficiency and effectiveness improvements
* Stay current on new revenue accounting rules and help develop appropriate policies and processes
* Provide revenue training to other groups within GitLab
* Liaison with External auditors, present significant item updates, supporting documentation for the quarterly reviews and year-end audits
* Lead the ongoing revenue automation process, and work with IT to prioritize further potential system enhancements and development projects related to revenue

#### Director, Revenue Accounting Requirements

* CPA is required, with at least 12 years of professional experience including significant management, and technology industry experience
* A combination of public company experience in a revenue leadership role and experience with public accounting firms
* Polished communication skills, including an ability to listen to the needs of the business units, research and comprehend complex matters, articulate issues in a clear and simplified manner, and present findings and recommendations in both oral and written presentations
* Strong ability to work well within a team structure and an ability to influence, train, mentor and leverage the skills of others to achieve objectives
* Experience with ERP systems and processes and with commission and billing systems
* Ability to use GitLab

### Senior Director, Revenue 

The Senior Director, Revenue Accounting reports to the Principal Accounting Offer (PAO).

#### Senior Director, Revenue Accounting Job Grade

The Senior Director, Revenue Accounting is a [grade 11](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Director, Revenue Accouting Responsibilities

* Guide, mentor and manage the world-wide revenue, billing and commission teams
* Trusted advisor to the business on commercial and go to market decisions
* Drive continuous scalability, automation and efficiency 
* Matrix management capabilities to communicate with and educate finance and non-finance personnel on revenue accounting guidance, Gitlab specific policies and procedures as well as inputs on structuring of customer arrangements
Review customer arrangements for proper revenue recognition in accordance with company policy and revenue recognition accounting standards
* Ensure the integrity of the world-wide license, support, service and training revenue and assist in new product offerings that may be developed over time
* Review license and service revenue related balance sheet account reconciliations
* Be a global partner with subject matter expertise in revenue and work diligently on finding solutions to critical issues
* Develop and maintain strong collaborative working relationships across Customer Service, Legal, Sales, FP&A Accounting and Tax teams
* Research and review technical revenue accounting memos that addresses the issue, and provide analysis and conclusions related to various customer contracts or new technical revenue pronouncements
* Ensure the integrity of key processes by understanding systems, flow of transactions, internal controls, and recommending efficiency and effectiveness improvements
* Stay current on new revenue accounting rules and help develop appropriate policies and processes
Provide revenue training to other groups within GitLab
* Liaison with External auditors, present significant item updates, supporting documentation for the quarterly reviews and year-end audits
* Lead the ongoing revenue automation process, and work with IT to prioritize further potential system enhancements and development projects related to revenue

#### Senior Director, Revenue Accounting Requirements

* CPA is required, with 12-15 years of professional experience including significant management, and technology industry experience
* A combination of public company experience in a revenue leadership role and experience with public accounting firms
* Polished communication skills, including an ability to listen to the needs of the business units, research and comprehend complex matters, articulate issues in a clear and simplified manner, and present findings and recommendations in both oral and written presentations
* Strong ability to work well within a team structure and an ability to influence, train, mentor and leverage the skills of others to achieve objectives
* Experience with ERP systems and processes and with commission and billing systems
* Ability to use GitLab

### Performance Indicators

* [Deals reviewed for revenue recognition = 100%](/handbook/finance/accounting/#deals-reviewed-for-revenue-recognition--100)
* [Non GAAP Revenue (Ratable Recognition)](/handbook/finance/accounting/#non-gaap-revenue-ratable-recognition)
* [Revenue recognition and accounting for other quote to cash transactions in Net Suite](/handbook/finance/accounting/#8-revenue-recognition-and-accounting-for-other-quote-to-cash-transactions-in-net-suite)

## Career Ladder

The next step in the Revenue Accounting job family is to move to the [Principal Accounting Officer](/job-families/finance/pao-jf/) job family. 
## Hiring Process

Candidates for the Revenue Accounting team can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

- Selected candidates will be invited to schedule a 30 minute screening call with our Global Recruiters
- Next, candidates will be invited to meet with the Director of Revenue
- Next, candidates will be invited to schedule a 45 minute interview with our Controller and one other member of the Accounting team
- Finally, candidates may be asked to interview with the Principal Accounting Officer
- Successful candidates will subsequently be made an offer via email or phone call

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing/)
